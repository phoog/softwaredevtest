﻿using NUnit.Framework;
using System.Collections.Generic;

namespace SharpBank.Test
{
    [TestFixture]
    public class InterestCalculatorTest
    {
        [Test]
        public void InterestCalculatorSingleBracket()
        {
            const decimal rate = 0.02m;
            const decimal balance = 100m;

            var rates = new SortedDictionary<decimal, decimal> { { decimal.MaxValue, rate } };

            var target = new InterestCalculator(rates);
            var actual = target.CalculateInterest(GetTransactions(100.0m));
            var expected = rate * balance;

            Assert.That(actual, Is.EqualTo(expected));
        }

        private IEnumerable<Transaction> GetTransactions(decimal sum)
        {
            return new Transaction[] { new Transaction(sum) };
        }

        [Test]
        public void InterestCalculatorLowerBracketOf2()
        {
            InterestCalculatorDoubleBracketTestHelper(100, 2);
        }

        [Test]
        public void InterestCalculatorBracketThreshold()
        {
            InterestCalculatorDoubleBracketTestHelper(1000, 20);
        }

        [Test]
        public void InterestCalculatorUpperBracketOf2()
        {
            InterestCalculatorDoubleBracketTestHelper(1100, 25);
        }

        private void InterestCalculatorDoubleBracketTestHelper(decimal amount, decimal expectedInterest)
        {
            decimal lowRate = 0.02m;
            decimal highRate = 0.05m;

            var target = new InterestCalculator(new SortedDictionary<decimal, decimal> {
                { 1000.0m, lowRate },
                { decimal.MaxValue, highRate }
            });

            var actualInterest = target.CalculateInterest(GetTransactions(amount));

            Assert.That(actualInterest, Is.EqualTo(expectedInterest));
        }

        [Test]
        public void InterestCalculatorLowerBracketOf3()
        {
            InterestCalculatorTripleBracketTestHelper(500, 15);
        }

        [Test]
        public void InterestCalculatorMiddleBracket()
        {
            const decimal lowerBracket = 10000.0m * 0.03m;
            const decimal middleBracket = 2000.0m * 0.07m;
            const decimal expectedResult = lowerBracket + middleBracket;

            InterestCalculatorTripleBracketTestHelper(12000, expectedResult);
        }

        [Test]
        public void InterestCalculatorUpperBracket()
        {
            const decimal lowerBracket = 10000.0m * 0.03m;
            const decimal middleBracket = 10000.0m * 0.07m;
            const decimal topBracket = 50000.0m * 0.11m;

            const decimal expectedResult = lowerBracket + middleBracket + topBracket;

            InterestCalculatorTripleBracketTestHelper(70000, expectedResult);
        }

        private void InterestCalculatorTripleBracketTestHelper(decimal amount, decimal expectedInterest)
        {
            const decimal lowRate = 0.03m;
            const decimal middleRate = 0.07m;
            const decimal highRate = 0.11m;

            var target = new InterestCalculator(new SortedDictionary<decimal, decimal> {
                { 10000.0m, lowRate },
                { 20000.0m, middleRate },
                { decimal.MaxValue, highRate }
            });

            var actualInterest = target.CalculateInterest(GetTransactions(amount));
            Assert.That(actualInterest, Is.EqualTo(expectedInterest));
        }
    }
}