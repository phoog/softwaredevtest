﻿using NUnit.Framework;
using System;
using System.Linq;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        [Test]
        public void CustomerConstructor()
        {
            const string name = "CustomerName";
            var target = new Customer(name);
            Assert.That(target.Name, Is.EqualTo(name));
            Assert.That(target.Accounts, Is.Empty);
        }

        [Test]
        public void TestOneAccount()
        {
            TestAccountHelper(1);
        }

        [Test]
        public void TestTwoAccount()
        {
            TestAccountHelper(2);
        }

        [Test]
        public void TestThreeAcounts()
        {
            TestAccountHelper(3);
        }

        private void TestAccountHelper(int count)
        {
            var accountTypes = new AccountType[] {
                AccountType.Savings,
                AccountType.Checking,
                AccountType.MaxiSavings
            };

            ValidateRangeThrowHelper(nameof(count), count, 1, accountTypes.Length);

            Customer oscar = new Customer("Oscar");

            foreach (var accountType in accountTypes.Take(count))
                oscar.OpenAccount(new Account(accountType));

            Assert.That(oscar.Accounts.Count, Is.EqualTo(count));
        }

        private void ValidateRangeThrowHelper(string parameterName, int parameterValue, int minimumValue, int maximumValue)
        {
            if (parameterValue < minimumValue || parameterValue > maximumValue)
            {
                var message = "Value must be between " + minimumValue + " and " + maximumValue + ", inclusive.";
                throw new ArgumentOutOfRangeException(parameterName, parameterValue, message);
            }
        }

        [Test]
        public void TransferAmountMustBeNonzero()
        {
            var target = new Customer(null);

            var myAccount = new Account(AccountType.Checking);
            var myOtherAccount = new Account(AccountType.Savings);

            target.OpenAccount(myAccount);
            target.OpenAccount(myOtherAccount);

            Assert.That(() => target.Transfer(myAccount, myOtherAccount, 0)
                , Throws.InstanceOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void TransferAmountMustBeNonNegative()
        {
            var target = new Customer(null);

            var myAccount = new Account(AccountType.Checking);
            var myOtherAccount = new Account(AccountType.Savings);

            target.OpenAccount(myAccount);
            target.OpenAccount(myOtherAccount);

            Assert.That(() => target.Transfer(myAccount, myOtherAccount, -1)
                , Throws.InstanceOf<ArgumentOutOfRangeException>());
        }

        [Test]
        public void TransferValidatesOwnership()
        {
            var target = new Customer(null);

            var myAccount = new Account(AccountType.Checking);
            var somebodysAccount = new Account(AccountType.Checking);

            target.OpenAccount(myAccount);

            Assert.That(() => target.Transfer(myAccount, somebodysAccount, 1)
                , Throws.InvalidOperationException);
        }

        [Test]
        public void TransferValidatesAccountsAreDifferent()
        {
            var target = new Customer(null);

            var myAccount = new Account(AccountType.Checking);

            Assert.That(() => target.Transfer(myAccount, myAccount, 1)
                , Throws.ArgumentException);
        }

        [Test]
        public void TransferYieldsExpectedBalances()
        {
            var target = new Customer(null);

            var fromAccount = new Account(AccountType.Checking);
            var toAccount = new Account(AccountType.Savings);

            target.OpenAccount(fromAccount);
            target.OpenAccount(toAccount);

            const decimal initialBalance = 12345;
            const decimal transferAmount = 678;
            const decimal finalFromBalance = initialBalance - transferAmount;

            fromAccount.Deposit(initialBalance);
            target.Transfer(fromAccount, toAccount, transferAmount);

            Assert.That(fromAccount.Balance, Is.EqualTo(finalFromBalance));
            Assert.That(toAccount.Balance, Is.EqualTo(transferAmount));
        }
    }
}
