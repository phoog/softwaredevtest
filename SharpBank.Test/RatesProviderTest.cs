﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class RatesProviderTest
    {
        [Test]
        public void RatesProviderChecking()
        {
            const double checkingRate = 0.001;

            var result = RatesProvider.Instance.GetRates(AccountType.Checking);

            Assert.That(result.Count, Is.EqualTo(1));

            Assert.That(result, Contains.Key(decimal.MaxValue));
            Assert.That(result[decimal.MaxValue], Is.EqualTo(checkingRate));
        }

        [Test]
        public void RatesProviderSavings()
        {
            const decimal lowerRate = 0.001m;
            const decimal upperRate = 0.002m;
            const decimal threshold = 1000;

            var result = RatesProvider.Instance.GetRates(AccountType.Savings);

            Assert.That(result, Has.Count.EqualTo(2));

            Assert.That(result, Contains.Key(threshold));
            Assert.That(result[threshold], Is.EqualTo(lowerRate));

            Assert.That(result, Contains.Key(decimal.MaxValue));
            Assert.That(result[decimal.MaxValue], Is.EqualTo(upperRate));
        }

        [Test]
        public void RatesProviderMaxiSavings()
        {
            const decimal lowerRate = 0.02m;
            const decimal middleRate = 0.05m;
            const decimal upperRate = 0.10m;
            const decimal lowerThreshold = 1000;
            const decimal higherThreshold = 2000;

            var result = RatesProvider.Instance.GetRates(AccountType.MaxiSavings);

            Assert.That(result, Has.Count.EqualTo(3));

            Assert.That(result, Contains.Key(lowerThreshold));
            Assert.That(result[lowerThreshold], Is.EqualTo(lowerRate));

            Assert.That(result, Contains.Key(higherThreshold));
            Assert.That(result[higherThreshold], Is.EqualTo(middleRate));

            Assert.That(result, Contains.Key(decimal.MaxValue));
            Assert.That(result[decimal.MaxValue], Is.EqualTo(upperRate));
        }
    }
}