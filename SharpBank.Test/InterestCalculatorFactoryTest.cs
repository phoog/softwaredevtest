﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class InterestCalculatorFactoryTest
    {
        [Test]
        public void InterestCalculatorForChecking()
        {
            var target = new InterestCalculatorFactory();
            var result = target.GetCalculator(AccountType.Checking);
            Assert.That(result, Is.InstanceOf<InterestCalculator>());
        }
        [Test]
        public void InterestCalculatorForSavings()
        {
            var target = new InterestCalculatorFactory();
            var result = target.GetCalculator(AccountType.Savings);
            Assert.That(result, Is.InstanceOf<InterestCalculator>());
        }
        [Test]
        public void InterestCalculatorForMaxiSavings()
        {
            var target = new InterestCalculatorFactory();
            var result = target.GetCalculator(AccountType.MaxiSavings);
            Assert.That(result, Is.InstanceOf<MaxiInterestCalculator>());
        }
    }
}