﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace SharpBank.Test
{
    [TestFixture]
    public class MaxiInterestCalculatorTest
    {
        [Test]
        public void MaxiInterestWithoutWithdrawals()
        {
            var target = new MaxiInterestCalculator();
            const decimal amount = 1000;
            const decimal rate = 0.05m;
            const decimal expectedInterest = amount * rate;

            var transactions = new List<Transaction>
            {
                new Transaction(amount, new DateTime(1900, 1, 1, 2, 3, 4))
            };

            Assert.That(target.CalculateInterest(transactions), Is.EqualTo(expectedInterest));
        }

        [Test]
        public void MaxiInterestWithRecentWithdrawals()
        {
            var target = new MaxiInterestCalculator();
            const decimal depositAmount = 1000;
            const decimal withdrawalAmount = -50;
            const decimal rate = 0.001m;
            const decimal expectedInterest = (depositAmount + withdrawalAmount) * rate;

            var transactions = new List<Transaction>
            {
                new Transaction(depositAmount, new DateTime(1900, 1, 1, 2, 3, 4)),
                new Transaction(withdrawalAmount, DateTime.Now)
            };

            Assert.That(target.CalculateInterest(transactions), Is.EqualTo(expectedInterest));
        }

        [Test]
        public void MaxiInterestWithOldWithdrawals()
        {
            var target = new MaxiInterestCalculator();
            const decimal depositAmount = 1000;
            const decimal withdrawalAmount = -150;
            const decimal rate = 0.05m;
            const decimal expectedInterest = (depositAmount + withdrawalAmount) * rate;

            var transactions = new List<Transaction>
            {
                new Transaction(depositAmount, new DateTime(1900, 1, 1, 2, 3, 4)),
                new Transaction(withdrawalAmount, new DateTime(1950, 1, 1, 2, 3, 4))
            };

            Assert.That(target.CalculateInterest(transactions), Is.EqualTo(expectedInterest));
        }
    }
}