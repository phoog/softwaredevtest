﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void TransactionAmount()
        {
            const decimal amount = 5;
            Transaction t = new Transaction(amount);
            Assert.That(t.Amount, Is.EqualTo(amount));
        }

        [Test]
        public void TransactionTime()
        {
            const decimal amount = 5;
            var dateTime = new DateTime(1950, 1, 1);
            Transaction t = new Transaction(amount, dateTime);
            Assert.That(t.Time, Is.EqualTo(dateTime));
        }
    }
}
