﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        [Test]
        public void BankConstructor()
        {
            var target = new Bank();

            Assert.That(target.Customers, Is.Empty);
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(AccountType.Checking);
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0m);

            Assert.That(bank.TotalInterestPaid(), Is.EqualTo(0.1m));
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(AccountType.Savings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(1500.0m);

            Assert.That(bank.TotalInterestPaid(), Is.EqualTo(2.0m));
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Account(AccountType.MaxiSavings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0m);

            Assert.That(bank.TotalInterestPaid(), Is.EqualTo(150.0));
        }
    }
}
