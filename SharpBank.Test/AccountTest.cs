﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTest
    {
        [Test]
        public void AccountConstructor()
        {
            const AccountType accountType = AccountType.Checking;

            var target = new Account(accountType);
            Assert.That(target.AccountType, Is.EqualTo(accountType));
            Assert.That(target.Balance, Is.EqualTo(0m));
        }

        [Test]
        public void AccountBalance()
        {
            const decimal deposit1 = 11m;
            const decimal deposit2 = 13m;
            const decimal withdrawal = 7m;
            const decimal expectedBalance = deposit1 + deposit2 - withdrawal;

            var target = new Account(AccountType.Checking);
            target.Deposit(deposit1);
            target.Deposit(deposit2);
            target.Withdraw(withdrawal);

            Assert.That(target.Balance, Is.EqualTo(expectedBalance));
        }
    }
}