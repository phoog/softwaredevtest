﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class ReportBuilderTest
    {
        public void CustomerStatement()
        {
            var customer = GetCustomerForStatement();

            var target = new ReportBuilder();

            Assert.That(target.GetStatementFor(customer), Is.EqualTo("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00"));
        }

        private Customer GetCustomerForStatement()
        {
            Account checkingAccount = new Account(AccountType.Checking);
            Account savingsAccount = new Account(AccountType.Savings);

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0m);
            savingsAccount.Deposit(4000.0m);
            savingsAccount.Withdraw(200.0m);

            return henry;
        }

        [Test]
        public void CustomerSummary()
        {
            var bank = GetBankForCustomerSummary();
            var target = new ReportBuilder();
            Assert.That(target.GetCustomerSummaryFor(bank), Is.EqualTo("Customer Summary\n - John (1 account)"));
        }

        private Bank GetBankForCustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new Account(AccountType.Checking));
            bank.AddCustomer(john);
            return bank;
        }
    }
}