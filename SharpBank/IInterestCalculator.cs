﻿using System.Collections.Generic;

namespace SharpBank
{
    public interface IInterestCalculator
    {
        decimal CalculateInterest(IEnumerable<Transaction> transactions);
    }
}