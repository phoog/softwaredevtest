﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBank
{
    public class Account
    {
        private readonly AccountType accountType;
        private readonly List<Transaction> transactions;
        private readonly IInterestCalculator interestCalculator;

        public Account(AccountType accountType)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
            this.interestCalculator = new InterestCalculatorFactory().GetCalculator(accountType);
        }

        public void Deposit(decimal amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(amount), "amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(decimal amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(amount), "amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
            }
        }

        public decimal InterestEarned()
        {
            return interestCalculator.CalculateInterest(Transactions);
        }

        public decimal Balance
        {
            get { return Transactions.Sum(t => t.Amount); }
        }

        public AccountType AccountType
        {
            get { return accountType; }
        }

        public IReadOnlyList<Transaction> Transactions
        {
            get { return transactions.AsReadOnly(); }
        }
    }
}
