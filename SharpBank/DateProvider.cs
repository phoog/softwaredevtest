﻿using System;

namespace SharpBank
{
    public class DateProvider
    {
        private static readonly Lazy<DateProvider> instance
            = new Lazy<DateProvider>(() => new DateProvider());

        public static DateProvider Instance
        {
            get { return instance.Value; }
        }

        public DateTime Now
        {
            get { return DateTime.Now; }
        }
    }
}
