﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Bank
    {
        private List<Customer> customers;

        public Bank()
        {
            customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
        }

        public IReadOnlyList<Customer> Customers
        {
            get { return customers.AsReadOnly(); }
        }

        public decimal TotalInterestPaid()
        {
            decimal total = 0;
            foreach (Customer c in customers)
                total += c.TotalInterestEarned();
            return total;
        }
    }
}
