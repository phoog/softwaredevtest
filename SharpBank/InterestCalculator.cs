﻿using System.Collections.Generic;
using System.Linq;

namespace SharpBank
{
    public class InterestCalculator : IInterestCalculator
    {
        private readonly SortedDictionary<decimal, decimal> interestRates;

        public InterestCalculator(SortedDictionary<decimal, decimal> interestRates)
        {
            this.interestRates = interestRates;
        }

        public decimal CalculateInterest(IEnumerable<Transaction> transactions)
        {
            var amount = transactions.Sum(t => t.Amount);

            var result = 0.0m;
            var previousThreshold = 0.0m;

            foreach (var bracket in interestRates)
            {
                if (bracket.Key < amount)
                {
                    result += (bracket.Key - previousThreshold) * bracket.Value;
                    previousThreshold = bracket.Key;
                }
                else
                {
                    result += (amount - previousThreshold) * bracket.Value;
                    break;
                }
            }

            return result;
        }
    }
}