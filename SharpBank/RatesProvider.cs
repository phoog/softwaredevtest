﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class RatesProvider
    {
        private static readonly Lazy<RatesProvider> instance
            = new Lazy<RatesProvider>(() => new RatesProvider());

        public static RatesProvider Instance { get { return instance.Value; } }

        private readonly SortedDictionary<decimal, decimal> _checking =
            new SortedDictionary<decimal, decimal> {
                { decimal.MaxValue, 0.001m }
            };

        private readonly SortedDictionary<decimal, decimal> _savings =
            new SortedDictionary<decimal, decimal> {
                { 1000.0m, 0.001m },
                { decimal.MaxValue, 0.002m }
            };

        private readonly SortedDictionary<decimal, decimal> _maxiSavings =
            new SortedDictionary<decimal, decimal> {
                { 1000.0m, 0.02m },
                { 2000.0m, 0.05m },
                { decimal.MaxValue, 0.1m }
            };

        private RatesProvider()
        {

        }

        public SortedDictionary<decimal, decimal> GetRates(AccountType accountType)
        {
            switch (accountType)
            {
                case AccountType.Checking:
                    return _checking;
                case AccountType.Savings:
                    return _savings;
                case AccountType.MaxiSavings:
                    return _maxiSavings;
                default:
                    throw new ArgumentException("Invalid account type");
            }
        }
    }
}