﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String Name
        {
            get { return name; }
        }

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        public IReadOnlyList<Account> Accounts { get { return accounts.AsReadOnly(); } }

        public decimal TotalInterestEarned()
        {
            decimal total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();
            return total;
        }

        public void Transfer(Account fromAccount, Account toAccount, decimal amount)
        {
            if (fromAccount == toAccount)
                throw new ArgumentException("From and to accounts must be different");

            if (!accounts.Contains(fromAccount) || !accounts.Contains(toAccount))
                throw new InvalidOperationException("Customer does not own both accounts");

            fromAccount.Withdraw(amount);
            toAccount.Deposit(amount);
        }
    }
}
