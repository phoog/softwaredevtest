﻿using System;
using System.Text;

namespace SharpBank
{
    public class ReportBuilder
    {
        public string GetStatementFor(Customer customer)
        {
            var stringBuilder = new StringBuilder("Statement for ")
                .Append(customer.Name)
                .Append('\n');

            decimal total = 0.0m;

            foreach (Account a in customer.Accounts)
            {
                stringBuilder.Append('\n');
                AppendAccountStatement(a, stringBuilder);
                stringBuilder.Append('\n');

                total += a.Balance;
            }

            stringBuilder.Append("\nTotal In All Accounts ");
            AppendAmountInDollars(total, stringBuilder);

            return stringBuilder.ToString();
        }

        private void AppendAccountStatement(Account account, StringBuilder stringBuilder)
        {
            stringBuilder.Append(PrettyAccountTypeName(account.AccountType))
                .Append('\n');

            //Now total up all the transactions
            decimal total = 0.0m;
            foreach (Transaction t in account.Transactions)
            {
                stringBuilder.Append("  ")
                    .Append(t.Amount < 0 ? "withdrawal" : "deposit")
                    .Append(' ');
                AppendAmountInDollars(t.Amount, stringBuilder);
                stringBuilder.Append('\n');

                total += t.Amount;
            }
            stringBuilder.Append("Total ");
            AppendAmountInDollars(total, stringBuilder);
        }

        private string PrettyAccountTypeName(AccountType accountType)
        {
            switch (accountType)
            {
                case AccountType.Checking:
                    return "Checking Account";
                case AccountType.Savings:
                    return "Savings Account";
                case AccountType.MaxiSavings:
                    return "Maxi Savings Account";
            }
            throw new ArgumentOutOfRangeException("accountType");
        }

        private void AppendAmountInDollars(decimal amount, StringBuilder stringBuilder)
        {
            stringBuilder.AppendFormat("${0:N2}", Math.Abs(amount));
        }

        public string GetCustomerSummaryFor(Bank bank)
        {
            var stringBuilder = new StringBuilder("Customer Summary");
            foreach (Customer c in bank.Customers)
            {
                var accountCount = c.Accounts.Count;

                stringBuilder.Append("\n - ")
                    .Append(c.Name)
                    .Append(" (")
                    .Append(accountCount)
                    .Append(' ')
                    .Append(accountCount == 1 ? "account" : "accounts")
                    .Append(')');
            }
            return stringBuilder.ToString();
        }
    }
}
