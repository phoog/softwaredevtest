﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        private readonly decimal amount;

        private readonly DateTime transactionDateTime;

        public Transaction(decimal amount) : this(amount, DateProvider.Instance.Now) { }
        public Transaction(decimal amount, DateTime dateTime)
        {
            this.amount = amount;
            this.transactionDateTime = dateTime;
        }

        public decimal Amount { get { return amount; } }
        public DateTime Time { get { return transactionDateTime; } }
    }
}
