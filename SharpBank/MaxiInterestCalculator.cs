﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBank
{
    public class MaxiInterestCalculator : IInterestCalculator
    {
        public decimal CalculateInterest(IEnumerable<Transaction> transactions)
        {
            var totalAmount = transactions.Sum(t => t.Amount);
            var rate = GetRate(transactions);
            return rate * totalAmount;
        }

        private decimal GetRate(IEnumerable<Transaction> transactions)
        {
            const decimal highRate = 0.05m;
            const decimal lowRate = 0.001m;

            var withdrawals = transactions.Where(t => t.Amount < 0).ToList();

            if (withdrawals.Count == 0)
                return highRate;

            var mostRecentWithdrawalDate = withdrawals.Max(t => t.Time);

            var cutoffDate = DateProvider.Instance.Now.Date - TimeSpan.FromDays(10);

            return mostRecentWithdrawalDate.Date <= cutoffDate
                ? highRate
                : lowRate;
        }
    }
}