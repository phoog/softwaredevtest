﻿using System;

namespace SharpBank
{
    public class InterestCalculatorFactory
    {
        public IInterestCalculator GetCalculator(AccountType accountType)
        {
            return accountType == AccountType.MaxiSavings
                ? (IInterestCalculator)new MaxiInterestCalculator()
                : new InterestCalculator(RatesProvider.Instance.GetRates(accountType));
        }
    }
}
