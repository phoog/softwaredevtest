﻿namespace SharpBank
{
    public enum AccountType
    {
        Checking = 1,
        Savings,
        MaxiSavings
    }
}